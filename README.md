# ansible-ec2-refresh #

This playbook uses the Ansible EC2 module to interact with AWS and "refresh" instances back to their original AMI state. When the playbook runs:

1. Ansible uses the EC2 module to poll AWS for instances that match the tags defined in the `ec2count_tags` variable
2. All instances in the matching group are terminated, if any
3. A number of instances are created, which are configured to match the criteria for the previously-terminated instances:
    - Access keys
    - AMI
    - Region, VPC & Subnet
    - Security groups & Instance Profile
    - Instance type
    - Tags
    - Count
4. Post-tasks (user setup, etc.) are performed to configure the newly-minted instances

The result is a set of AWS instances that should reflect the state of a previously-defined group of AWS instances, freshly-installed on a new AMI.

### Requirements ###

- Ansible (this script was tested with Ansible 2.2.0)
- Python and all the trimmings necessary to run virtualenv, along with the virtualenv software itself
- A plain-text password file to unencrypt the "vaulted" variables, `~/.vault_pass` in this example
- An SSH key pair with permissions to interact with EC2, etc.

### Usage ###

- Set up virtualenv:

```
    $ mkdir /some/working/folder
    $ cd /some/working/folder
    $ virtualenv env_folder
```

- Clone the code and activate the virtualenv to install requirements:

```
    $ cd /code/folder
    $ git clone https://bitbucket.org/slaurvick/ansible-devops
    $ source /path/to/env_folder/bin/activate

    (env_folder)$ pip install -r requirements.txt
```

- Edit `./env/testing/group_vars/localhost.yml` and change the following to the appropriate values for your project:

    - `ec2instance_keyname`: If the instances already exist, this is the name of the key pair used to log in. Otherwise, this key pair will be used to create the new instances.
    - `ec2instance_region`: Amazon AWS region where the instances reside.
    - `ec2instance_profile`: Name of the IAM role which will terminate / create the new instances.
    - `ec2instance_count`: Integer value for the number of instances in this group; Changing this value and re-running the playbook will shrink/grow the instance group accordingly.
    - `ec2instance_vpc_subnet`: Subnet name where the instances reside.
    - `ec2instance_host_groupname`: Security group in which the instances reside.
    - `ec2instance_volume_size`: Size (in Gigabytes) of the instance root volume, used for all instances in the group.
    - `ec2count_tags`: Series of tags on which to match instances.

- Edit `./env/testing/inventory` with the appropriate path to your virtualenv Python executable. Use `which python` once your environment is activated.

- Run the playbook:
    - Use the inventory file that works best for your environment, e.g. if your environment were "development", you would run: 
        `ansible-playbook -i env/development site.yml --vault-password-file=~/.vault_pass`
    - It may be necessary to add ` --ssh-extra-args="-o ControlPath=none" --sftp-extra-args="-o ControlPath=none"` if you have problems with connection timeouts.

### Issues ###

- Currently the script shows some odd behavior on the "update and restart" portion, especially when working with a newly-minted instance. The script currently updates the instance as it should, but the "reboot" command does not reboot the instance (even though Ansible reports "OK"). Currently this is mitigated by simply logging into the instance(s) just created and then restarting them manually.
- Upon first login, a human is needed to enter "yes" at the SSH key verification prompt. Since this playbook will terminate instances and create them each time, this tends to happen every time the playbook is run. Either a human can enter "yes" for each instance in the group, or you can set the option to disable strict checking somewhere in your `ansible.cfg`:
    ```
    [defaults]
    host_key_checking = False
    ```

### Questions? ###

- Contact [Sean](https://blinkx.atlassian.net/wiki/spaces/~slaurvick) for any questions about the scripts in this repo.